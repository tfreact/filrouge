import React from "react";
import PropTypes from 'prop-types';
import style from "./Addition.module.css";

const Addition = function(props)
{
    const{ NB1,NB2}=props;
    
    if(NB1 < 0 || NB2<0)
    {
        return(<div className={style.AdditionError}>"Oops. Il y 'a une erreur.Veuillez entrer deux nombres positifs</div>)
    }
    else
    {
        if(!Number.isInteger(NB1) || !Number.isInteger(NB2))
        {
            return(<div className={style.AdditionError}>"Oops. Il y 'a une erreur.Veuillez entrer deux nombres entiers</div>)
        }
        else
        {
            let somme = NB1+NB2;
            return(<p>{NB1} + {NB2} = {somme}</p>)
        }
    }

}

Addition.propTypes=
{
    NB1:PropTypes.number,
    NB2:PropTypes.number
}

export default Addition;