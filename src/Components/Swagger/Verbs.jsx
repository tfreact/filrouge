import React from "react";
import styles from "./Swagger.module.css"
import Vstyles from "./verbs.module.css"
import classNames from 'classnames/bind';
const Verbs = function(props)
{ 

    //Permet de passer de bgRed à verbs_bgRed_45Z2
    let cx = classNames.bind(Vstyles);// transforme mon "fichier css" en un tableau d'élément css
    let className = {};
    if(props.info.method==="get") className =cx({btnVerbs:true, getBg: true});    
    if(props.info.method==="post") className =cx({btnVerbs:true, postBg: true});    
    if(props.info.method==="put") className =cx({btnVerbs:true, putBg: true});  
    if(props.info.method==="delete") className = cx({btnVerbs:true, delBg: true});



    return(
        <span>
        <div className={[styles.opblock]} >
            <div className={styles.opblocksummary}> 
                                            <span className={[styles.button,className]} > 
                                                {props.info.method}  
                                                </span> 
                                        
                <span className={styles.opblocksummarypath} data-path={props.info.title}>
                    <a className={styles.nostyle} href={props.info.title}><span>{props.info.title}</span>
                    </a>
                </span>
                <div className={styles.opblocksummarydescription}>{props.info.description}</div>
            </div>
        </div>
        </span>
    );
};

export default Verbs;