import React from "react";
import PropTypes from 'prop-types';
import style from "./Person.module.css";

const Person = function(props)
{  if(props.Age<18)
    {
    return(
      
        <div className={style.PersonRedBox}>
            <p>Je suis {props.Nom} et
                j'ai {props.Age} ans !
            </p>
        </div>
    );
    }
    else
    {
        return(
      
            <div className={style.PersonBox}>
                <p>Je suis {props.Nom} et
                    j'ai {props.Age} ans !
                </p>
            </div>
        );
    }
};

Person.propTypes=
{
    Nom:PropTypes.string,
    Age:PropTypes.number
};

export default Person;