import { TASK_FINISHED, TASK_ADDED, TASK_DELETED } from "../actions/action-task";

// Valeur initial du store 
const initialState = {
    activeTask: null,
    tasks: [] 
}

// Fonction "Reducer"
const TodoReducer = (state = initialState, action) => {
    switch(action.type) {
        case TASK_ADDED:
            return {
                ...state,
                tasks: [...state.tasks,action.payload]
            }
        case TASK_FINISHED:
                return {
                    ...state,
                    activeBook: action.payload
                }
        case TASK_DELETED:
                    return {
                        ...state,
                        activeBook: action.payload
                    }
        default:
            return state;
    }
}

export default TodoReducer;