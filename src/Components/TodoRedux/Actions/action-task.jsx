// Nom des actions possible
export const TASK_ADDED = 'TASK_ADDED';
export const TASK_DELETED = 'TASK_DELETED';
export const TASK_FINISHED = 'TASK_FINISHED';

// Les méthodes d'action
export const AddTask = (task,u) => ({
    type: TASK_ADDED,
    payload: task
});

export const DelTask = (task) => ({
    type: TASK_DELETED,
    payload: task
});

export const FinishTask = (task) => ({
    type: TASK_FINISHED,
    payload: task
});

 