import React from "react"; 
import ReactPeople from "../../datas/ReactCoreTeam.json";
import Card from "./card"
import shortid from 'shortid';

const SocialNetwork=function(props)
{
    return(
        <div>
        <h3>The Social Network</h3>
        {ReactPeople.map((profile) => (
          <Card key={shortid.generate()}  data={profile} />
        ))}
      </div>   
    
        );
}

export default SocialNetwork;