import React, {useState, useEffect} from 'react'
import Axios from 'axios'

const ApiUrl = "http://api.openweathermap.org/data/2.5/weather?q=__city__&APPID=3e900d3940e23fcfa3bb4d2e1db08a47&units=metric&lang=fr";

const MeteoListItem =(props)=>
{
    const {ville} = props;
    const [isLoaded, setLoaded] = useState(false); 
    const [error, setError] = useState(false);
    const [data, setData] = useState(null);

    useEffect(() => {
        Axios.get(ApiUrl.replace('__city__', ville))
        .then(({data}) => {
            setData({
                nom: data.name,
                pays: data.sys.country,
                temp: data.main.temp,
                desc: data.weather.reduce((acc, value) => [...acc, value.description], []).join(', ')
            });
            setLoaded(true);
        })
        .catch(error => {
            setError(true); 
            setLoaded(true);
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if(!isLoaded) {
        return (<li><img src="./images/loading.gif" alt="loading"/></li>)
    }

    if(error) {
        return (
        <li>
            <img src="./images/error.gif" alt="Error"/>
             <p>Erreur pour la ville : « {ville} »</p>
        </li>)
        
    }

    const {nom, pays, temp, desc} = data;

    return (
        <li>{nom} ({pays}) {temp}°c - {desc}</li>
    );
};

export default MeteoListItem;