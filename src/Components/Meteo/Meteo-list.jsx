import React from 'react'
import MeteoListItem from './Meteo-list-item';


const MeteoList = (props) => {

    const meteo = props.villes.map(
        Ville => <MeteoListItem key={Ville} ville={Ville}></MeteoListItem>
    );

    return (
        <ul>{meteo}</ul>
    );
}

export default MeteoList;