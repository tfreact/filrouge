import React, { Component, Fragment } from 'react'
import CitySearch from './City-search'
import MeteoList from './Meteo-list';

class Meteo extends Component
{
    constructor(props)
    {
        super(props);
        this.state={
            villes:[]
        }

        this.onSearch=this.onSearch.bind(this);
    }

    onSearch = (ville) => {
        if(!this.state.villes.includes(ville.toLowerCase())) {
          this.setState(s => ({
            villes: [...s.villes, ville.toLowerCase()]
          }));
        }
      }


    render()
    {
        return(
            <Fragment>                
                <h1>Meteo Component</h1>
                <CitySearch onSearch={this.onSearch}/>
                <hr/>
                <MeteoList villes={this.state.villes}></MeteoList>
            </Fragment>

        );
    }
}

export default Meteo;