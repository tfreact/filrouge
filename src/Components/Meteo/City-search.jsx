import React, { Component } from 'react' 

class CitySearch extends Component
{
    constructor(props)
    {
        super(props);
        this.state=(
            {
                ville:''
            });

            this.onChange= this.onChange.bind(this);
            this.onSubmit= this.onSubmit.bind(this);    
    }


    onChange=(e)=>
    { 
        const {value} = e.target; 
        this.setState({
            ville: value
        });
    }

    onSubmit=(e)=>
    {
        e.preventDefault();

        this.props.onSearch(this.state.ville);

        this.setState({
            ville: ''
        });
    }

    render()
    { 
        return (
            <form onSubmit={this.onSubmit}>
                <input type='text' value={this.state.ville} onChange={this.onChange} />
                <input type='submit' value="Rechercher" />
            </form>
        );
    }

    
    }

CitySearch.defaultProps = {
    onSubmit: () => {}
}

export default CitySearch;
