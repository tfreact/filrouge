import React, {Component} from 'react';

class StartupForm extends Component
{

    constructor(props)
    {
        super(props);

        this.state=
        {
            Nom:"Stranger",
            Ville:"",
            Comment:"",
            message:""

        };
        this.handleInput= this.handleInput.bind(this);
        this.onSubmit= this.onSubmit.bind(this);
    }

    onSubmit(e)
    {
        e.preventDefault();
        console.log(this.state);
        
        this.setState(
            {
            Nom:"Stranger",
            Ville:"",
            Comment:"",
            message:"Formulaire bien envoyé!"

        });

    }
    handleInput(e)
    {
        const {name, type, checked, value} = e.target;
        this.setState({[name]:type==='checkbox'?checked:value});
    } 

    render()
    {

        return(
        
            <div className="jumbotron">
                <h2>Hello {this.state.Nom}</h2>
                    <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="Nom">Votre nom</label>
                        <input className={"form-control"} type="text" name="Nom" value={this.state.Nom} onChange={this.handleInput}></input>
                        </div>
  <div className={"form-group"}>
                        
                        <label htmlFor="Ville">D'où venez-vous?</label>
                    <select  className={"form-control"} name="Ville" value={this.state.Ville} onChange={this.handleInput}>
                        <option value="Charleroi">Charleroi</option>
                        <option value="Namur">Namur</option>
                    </select>
                    </div>
  <div className={"form-group"}>
                    <label htmlFor="Comment">Un commentaire ?</label>
                        <textarea  className={"form-control"} name="Comment" value={this.state.Comment} onChange={this.handleInput}></textarea>
                        <br/>
                        </div>
  <div className={"form-group"}>
                    <button className={"btn btn-primary"}>Envoyer</button>
                    
                    </div>
                    </form>
                   
                    <div className={this.state.message !==""?"alert alert-info":""}>{this.state.message}</div>
            </div>
        );
    }

};
export default StartupForm;