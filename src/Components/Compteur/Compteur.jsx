import React, { Component } from 'react'

class Compteur extends Component
{
    constructor(props)
    {
        super(props);
        this.state ={ 'valeur':0};
        this.onIncrease = this.onIncrease.bind(this);
        this.onIncrease10 = this.onIncrease10.bind(this);
        this.onIncrease100 = this.onIncrease100.bind(this);
        this.onIncrease1000 = this.onIncrease1000.bind(this);
        this.onDouble=this.onDouble.bind(this);
        this.onReset=this.onReset.bind(this);
    }

    onIncrease()
    {
        this.setState((state,props)=>
        ({
            valeur: state.valeur+1
        }));
    }

    onIncrease10()
    {
        this.setState((state,props)=>
        ({
            valeur: state.valeur+10
        }));
    }

    onIncrease100()
    {
        this.setState((state,props)=>(
        {
            valeur: state.valeur+100
        }));
    }

    onIncrease1000()
    {
        this.setState((state,props)=>(
        {
            valeur: state.valeur+100
        }));
    }
     
    onDouble()
    {
        this.setState((state,props)=>
       ( {
            valeur: state.valeur*2
        }));
    }

    onReset()
    {
        this.setState((state,props)=>
        ({
            valeur: 0
        }));
    }

    render()
    {
        return(
            <div>
                <h1>Compteur</h1>
                <b>{this.state.valeur}</b><br/>   
                <button onClick={this.onReset} className={["btn btn-outline-dark"]}>Reset</button>
                <button onClick={this.onIncrease} className={["btn btn-outline-primary"]}>+1</button>
                <button onClick={this.onIncrease10} className={["btn btn-outline-secondary"]}>+10</button>
                <button onClick={this.onIncrease100} className={["btn btn-outline-success"]}>+100</button>
                <button onClick={this.onIncrease1000} className={["btn btn-outline-info"]}>+1000</button>
                <button onClick={this.onDouble} className={["btn btn-outline-danger"]}>x2</button>
            </div>
            );
    }

}

export default Compteur;