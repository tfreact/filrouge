import React, {Component} from 'react'

class DateDuJour extends Component
{
    constructor(props)
    {
        super(props);
        const formatter = new Intl.DateTimeFormat('pl', { month: 'long' });
        this.state={
            jour:new Date().getDate(),
            mois:formatter.format(new Date()),
            annee:new Date().getFullYear()
        };
    }

    render()
    {
          return(
          <h1>Nous sommes le {this.state.jour} {this.state.mois} {this.state.annee}</h1>
          );

    }
}

export default DateDuJour;