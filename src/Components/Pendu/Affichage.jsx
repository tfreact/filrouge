import React from 'react'
import shortid from 'shortid'
const Affichage = (props)=>
{
    const {Mot, LettreChoisies }= props; 
    
    const tiret ="_";

   

    return(
        <div>
            Mot à trouver :
        {
            Mot.map((m) => (         
                
                <p key={shortid.generate()} style={{display:"inline", marginRight:"5px", fontSize:"20px", fontWeight:"Bold"}}>
                    {LettreChoisies.indexOf(m)!==-1?m:tiret}</p>
           ))
        }  
      </div>
    )
}

export default Affichage;