import logo from './logo.svg';
import './App.css';
import Hello from './Components/Hello/Hello';
import Person from './Components/Person/Person';
import Addition from './Components/Addition/Addition';
import SocialNetwork from './Components/SocialNetwork/SocialNetwork';
import Swagger  from './Components/Swagger/Swagger';
import HeaderSwag from './Components/Swagger/Header';
import DateDuJour from './Components/DateDuJour/DateDuJour';
import Compteur from './Components/Compteur/Compteur';
import StartupForm from './Components/StartupForm/StartupForm';
import Todo from './Components/Todo/Todo';
import ColorButton from './Components/ColorButton/ColorButton';
import MagicDice from './Components/MagicDice/MagicDice';
import MagicDiceEffect from './Components/MagicDice/MagicDiceEffect';
import Meteo from './Components/Meteo/Meteo';
import Pendu from './Components/Pendu/Pendu';
import Tableau from './Components/Swagger/Tableau';
import Verbs from './Components/Swagger/Verbs';

 
export const PRIORITY_HIGH = '1';
export const PRIORITY_NORMAL = '2';
export const PRIORITY_LOW = '3';
export const PRIORITY_TEXT = new Map();
PRIORITY_TEXT.set(PRIORITY_LOW, 'Bas');
PRIORITY_TEXT.set(PRIORITY_NORMAL, 'Normal');
PRIORITY_TEXT.set(PRIORITY_HIGH, 'Urgent');

function App() {
  const monVerbs =  {
    "title": "/foos",
    "description": "Retrieves the collection of Foo Resources.",
    "method": "get"
  };
  return (
    <div className="App"> 
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <br/>
      <h1>Exercice 1</h1>
      <Hello></Hello>
      <hr/>
      <h1>Exercice 2</h1>
      <hr/>
      <Person Nom="Pierre" Age={32}></Person>
      <Person Nom="Paul" Age={54}></Person>
      <Person Nom="Jacques" Age={17}></Person>
      <Person Nom="Hugo" Age={35}></Person>
      <Person Nom="36" Age={53}></Person> 
      <br clear="both"/>
      <hr/>
      <h1>Exercice 3</h1>
      <hr/>
      <Addition NB1={12} NB2={56}></Addition>
      <Addition NB1={78585858} NB2={2525252}></Addition>
      <Addition NB1={1.2} NB2={56}></Addition>
      <Addition NB1={-15} NB2={45}></Addition>
      <Addition NB1={15} NB2={-45}></Addition>
      <br clear="both"/>
      <hr/>
      <h1>Exercice 4</h1>
      <hr/>
      <SocialNetwork/>
      <br clear="both"/>
      <hr/>
      <h1>Challenge</h1>
      <hr/>
      <Verbs info={monVerbs}/>
      <Swagger/>
      <hr/>
      <h1>Exercice 5</h1>
      <hr/>
      <DateDuJour/>
      <hr></hr>
      <h1>Exercice 6</h1>
      <Compteur/>
      <hr/>
      <h1>Exercice 7</h1>
      <hr/>
      <StartupForm/>
      <hr/>
      <h1>Exercice 8</h1>
      <hr/>
      <Todo/>
      <hr/>
      <h1>Exercice 9</h1>
      <hr/>
      <ColorButton texte="Oh la belle bleue!"/>
      <hr/>
      <h1>Exercice 10</h1>
      <hr/>
      <MagicDice/>
      <hr/>
      <hr/>
      <h1>Exercice 10 avec UseEffect</h1>
      <hr/>
      <MagicDiceEffect/>
      <hr/>
      <h1>Exercice 11</h1>
      <hr/>
      <Pendu/><hr/>
      <h1>Exercice 12</h1>
      <hr/>
      <Meteo></Meteo>
       
    </div>
  );
}

export default App;
